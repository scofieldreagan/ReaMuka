import React from 'react';

const LandingPage = () => {
  return (
    <div>
       <div className="home-page" id="top">
        <div className="hero-banner">
          <div className="row">
            <div className="hero-banner__left large-3 columns">
            </div>
            <div className="hero-banner__middle large-6 columns">
              <h1>
                On Time When You Need Us.
              </h1>
              <button className="button-large white" data-open="contact-modal">Get in touch</button>
              <div className="rwd-devices">
              </div>
            </div>
            <div className="hero-banner__right large-3 columns">
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default LandingPage;