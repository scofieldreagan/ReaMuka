import React from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import './App.css';
import LandingPage from './pages/landingPage'
import Nav from './components/nav'

function App() {
  return (
    <Router>
    <>
    <Nav />
    <div>
      <Switch>
        <Route exact path="/" component={LandingPage} />
      </Switch>
    </div>
    </>
    </Router>
  );
}

export default App;
